import './scss/style.scss'

import React from 'react'
import ReactDOM from 'react-dom'
import App from './app/App'

import HeaderBar from './components/header.bar/HeaderBar'
import MobileMenu from './components/mobile.menu/MobileMenu'
import MenuData from './data/menu-left'
import LargeSlider from './components/large.slider/LargeSlider'
import Container from './lib/Container'
import Component from './lib/Component'

const container = new Container()

/**
 * Initialize mobile menu in left
 * @type {MobileMenu}
 */
const menu = new MobileMenu(MenuData)
menu.render('mobile-menu')
menu.init()
container.add('mobile-menu', menu)

/**
 * Init header bar
 * @type {HeaderBar}
 */
const header = new HeaderBar(menu)
header.init()

const slider = new LargeSlider()
slider.init('large-slider')

console.info(Component.getContainer())

ReactDOM.render(
  <React.StrictMode>
    <App/>
  </React.StrictMode>,
  document.getElementById('root')
)
