import $ from 'jquery'
import Component from '../../lib/Component'

class HeaderBar extends Component {
  constructor (menu) {
    super()
    this.menuComponent = menu
  }

  init () {
    const menuBtn = document.getElementsByClassName('header__menu-btn').item(0)
    if (menuBtn) {
      menuBtn.onclick = () => {
        this.menuComponent.toggle()
      }
    }

    $('.header__search-btn').click(() => {
      $('.search-bar').toggle()
    })

    $('.search-bar__collapse').click(() => {
      $('.search-bar').toggle()
    })
  }
}

export default HeaderBar
