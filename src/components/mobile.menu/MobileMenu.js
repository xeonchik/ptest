import template from './template.ejs'
import $ from 'jquery'

class MobileMenu {
  constructor (items) {
    this.items = items
    this.id = undefined
  }

  render (id) {
    const result = template({
      items: this.items
    })
    this.id = id
    document.getElementById(id).innerHTML = result
  }

  init () {
    this.hide()
  }

  show () {
    $('#' + this.id).show()
    $('body').addClass('no-scroll')
  }

  hide () {
    $('#' + this.id).hide()
    $('body').removeClass('no-scroll')
  }

  toggle () {
    $('#' + this.id).toggle()
    if ($('#' + this.id).is(':visible')) {
      $('body').addClass('no-scroll')
    } else {
      $('body').removeClass('no-scroll')
    }
  }
}

export default MobileMenu
