import 'tiny-slider/dist/tiny-slider.css'
import { tns } from 'tiny-slider/src/tiny-slider'
import Component from '../../lib/Component'

class LargeSlider extends Component {
  constructor () {
    super()
    this.name = 'LargeSlider'
  }

  init (id) {
    tns({
      container: '#' + id,
      items: 1,
      slideBy: 'page',
      controls: true,
      autoplay: true
    })
  }
}

export default LargeSlider
