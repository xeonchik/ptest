export default [
  { name: 'Новинки', icon: 'new' },
  { name: 'Корея', icon: 'korea' },
  { name: 'Подарки', icon: 'gift' },
  { name: 'Макияж', icon: 'makeup' }
]
