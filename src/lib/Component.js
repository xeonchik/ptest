import Container from './Container'

class Component {
  constructor () {
    this.name = 'BaseComponent'
    const container = Component.getContainer()
    container.add(this)
  }

  render () {
    return document.createElement('div')
  }

  mount (id) {
    this.id = id
    document.getElementById(id).replaceWith(this.render())
  }

  static getContainer () {
    if (Component.container) {
      return Component.container
    }
    Component.container = new Container()
    return Component.container
  }
}

export default Component
