class Container {
  constructor () {
    this.components = []
  }

  add (component) {
    this.components.push(component)
  }
}

export default Container
