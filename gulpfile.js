'use strict'

const gulp = require('gulp')
const del = require('del')

require('dotenv').config()
const deployPath = process.env.DEPLOY_SCRIPT_PATH

if (!deployPath) {
  console.error('DEPLOY_SCRIPT_PATH does not set in .env file')
  process.exit(1)
}

gulp.task('deploy_clean', () => {
  return del([
    deployPath + '/js',
    deployPath + '/css',
    deployPath + '/assets',
  ], { force: true })
})

gulp.task('deploy_copy_js', (done) => {
  return gulp.src('dist/js/*.js').pipe(gulp.dest(deployPath + '/js'))
})

gulp.task('deploy_copy_css', (done) => {
  return gulp.src('dist/css/*.css').pipe(gulp.dest(deployPath + '/css'))
})

gulp.task('deploy_copy_img', (done) => {
  return gulp.src('dist/assets/**/*').pipe(gulp.dest(deployPath + '/assets'))
})

gulp.task('deploy_copy_fonts', (done) => {
  return gulp.src('dist/fonts/**/*').pipe(gulp.dest(deployPath + '/fonts'))
})

gulp.task('deploy', gulp.series('deploy_clean', gulp.parallel('deploy_copy_js', 'deploy_copy_css', 'deploy_copy_img' /*'deploy_copy_fonts'*/)))
