const path = require('path')
const MiniCss = require('mini-css-extract-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')

module.exports = {
  entry: {
    app: './src/index.js'
  },

  output: {
    publicPath: '/',
    filename: 'js/[name].js',
    path: path.resolve(__dirname, 'dist')
  },

  devtool: 'source-map',

  devServer: {
    contentBase: './dist',
    hot: true,
    proxy: {
      '/upload': 'http://podrujkashop.test'
    }
  },

  module: {
    rules: [
      {
        test: /\.ejs$/,
        use: [
          'ejs-compiled-loader'
        ]
      },
      {
        test: /\.m?js$/,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env']
          }
        }
      },
      {
        test: /\.(sass|scss|css)$/,
        use: [
          MiniCss.loader,
          'css-loader',
          'sass-loader'
        ]
      },
      {
        test: /\.(png|svg|jpg|gif)$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: 'assets/[name].[ext]'
            }
          }
        ]
      }
    ]
  },

  optimization: {
    runtimeChunk: 'single',
    splitChunks: {
      cacheGroups: {
        vendor: {
          test: /[\\/]node_modules[\\/]/,
          name: 'vendors',
          chunks: 'all'
        }
      }
    }
  },

  plugins: [
    new MiniCss({
      filename: 'css/style.css'
    }),
    new HtmlWebpackPlugin({
      template: 'public/index.html'
    }),
    new HtmlWebpackPlugin({
      filename: 'catalog.html',
      template: 'public/catalog.html'
    })
  ]
}
